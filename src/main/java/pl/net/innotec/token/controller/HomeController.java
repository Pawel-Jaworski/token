package pl.net.innotec.token.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/")
    public String start() {
        return "Home page";
    }

    @GetMapping("/hello")
    public String hello() {
        return "Hello page only for authenticated users";
    }
}
