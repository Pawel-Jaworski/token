package pl.net.innotec.token.config;

public class SecurityConstants {
    public static final String SECRET = "secret";
    public static final long EXPIRATION_TIME =  8 * 60 * 60 * 1000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}